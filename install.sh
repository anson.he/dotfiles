#!/bin/bash

if [ -n "$CODER" ]; then
  # mv /home/coder/.gitconfig /home/coder/.gitconfig.bak
  ln -s /home/coder/.config/coderv2/dotfiles/.gitconfig /home/coder/.gitconfig
  ln -s /home/coder/.config/coderv2/dotfiles/.bash_aliases /home/coder/.bash_aliases
  # cp /home/coder/.config/coderv2/dotfiles/hooks.sh /workspace/remote-dev/hooks.sh
fi

curl -fsSL https://bun.sh/install | bash
export BUN_INSTALL="/home/YOUR_USERNAME/.bun"
export PATH="$BUN_INSTALL/bin:$PATH"
