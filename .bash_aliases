alias b2m="git checkout master && git pull --rebase origin master"
alias gpo="git push origin"
alias gpfo="git push -f origin"
alias grm="git rebase master"
alias gcm="git commit -m"
alias gcam="git commit -am"
alias gcb="git checkout -b"
alias gco="git checkout"
alias switch-to-staging="aws --profile staging-eks eks update-kubeconfig --name snaptravel-staging-eks"
alias switch-to-production="aws --profile production-eks eks update-kubeconfig --name snaptravel-production-eks"